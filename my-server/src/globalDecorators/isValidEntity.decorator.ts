import { Injectable } from '@nestjs/common';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidationArguments,
    ValidatorConstraintInterface,
} from 'class-validator';
import { DataSource } from 'typeorm';
import { EntityClassOrSchema } from '@nestjs/typeorm/dist/interfaces/entity-class-or-schema.type';



@Injectable()
@ValidatorConstraint({ async: true })
export class IsValidIdConstraint implements ValidatorConstraintInterface {

    constructor(private dataSource: DataSource) { }

    defaultMessage(validationArguments?: ValidationArguments): string {
        const constraints = validationArguments.constraints[0];
        return constraints.errorMsg;
    }

    async validate(value: number, args: ValidationArguments) {

        const constraints = args.constraints[0];

        const entity: EntityClassOrSchema = constraints.entity;

        const options = {};
        options[args.property] = value;

        return this.dataSource
            .createQueryBuilder(entity, constraints.tableName)
            .where(options)
            .getOne()
            .then(r => { return !!r; });
    };
}


export function isValidEntity(options: any, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [ options ],
            validator: IsValidIdConstraint
        });
    };
}
