import { Module } from '@nestjs/common';
import { EmployeesModule } from './employees/employees.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { typeOrmConfig } from './ormConfig';


@Module({
  imports: [
    AuthModule,
    EmployeesModule,
    TypeOrmModule.forRoot(typeOrmConfig)
  ]
})
export class AppModule { }
