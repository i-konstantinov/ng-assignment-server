import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { useContainer } from "class-validator";
import "reflect-metadata";



async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.setGlobalPrefix('api');
  
  app.enableCors({
    origin: "*",
    methods: ['GET', 'POST', 'PUT', 'DELETE', 'HEAD', 'OPTIONS'],
    allowedHeaders: 'Content-Type, my-access-token'
  });
  
  app.useGlobalPipes(new ValidationPipe());
  
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  
  await app.listen(3000);
}
bootstrap();
