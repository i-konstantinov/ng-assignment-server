import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { jwtConstants } from "src/auth/constants";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, "jwt-access") {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromHeader("my-access-token"),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.secret
        });
    }

    async validate(payload: any) {
        return { id: payload.id, username: payload.username }
    }
}