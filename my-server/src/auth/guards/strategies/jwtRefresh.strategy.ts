import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { ExtractJwt, Strategy } from "passport-jwt";
import { jwtConstants } from "src/auth/constants";


@Injectable()
export class JwtRefreshStrategy extends PassportStrategy(Strategy, "jwt-refresh") {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromBodyField("refreshToken"),
            ignoreExpiration: false,
            secretOrKey: jwtConstants.refreshSecret,
            passReqToCallback: true
        });
    }

    async validate(req: Request, payload: any) {
        const tokenStr = req.body.refreshToken;
        return { id: payload.id, username: payload.username, token: tokenStr }
    }
}