import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { RegisteredUserCredentials } from 'src/auth/types/dtos/registeredUserCredentials.dto';
import { UserEntity } from 'src/auth/types/typeorm/user.entity';
import { jwtConstants } from 'src/auth/constants';
import { Repository } from 'typeorm';
import { TokenEntity } from 'src/auth/types/typeorm/token.entity';
import { NewUserCredentials } from 'src/auth/types/dtos/newUserCredentials.dto';


@Injectable()
export class AuthService {

    constructor(
        @InjectRepository(UserEntity) private readonly registryRepository: Repository<UserEntity>,
        @InjectRepository(TokenEntity) private readonly tokenRepository: Repository<TokenEntity>,
        private jwtService: JwtService
    ) { }


    async registerUser(user: NewUserCredentials) {
        const newUser = await this.registryRepository.save(user);

        const { accessToken, refreshToken } = await this.getSignedTokens(newUser.id, newUser.username);

        this.tokenRepository.save({
            refreshToken: refreshToken,
            userId: newUser.id
        });

        console.log(`User: ${newUser.username} saved and logged in`);

        return {
            user: {
                id: newUser.id,
                username: newUser.username
            },
            tokens: {
                accessToken,
                refreshToken
            }
        };
    }


    async loginUser(data: RegisteredUserCredentials) {

        const user = await this.registryRepository.findOne({ where: { username: data.username }});
        
        const { accessToken, refreshToken } = await this.getSignedTokens(user.id, user.username);

        this.updateTokenTable(user.id, refreshToken);

        console.log(`User: ${user.username} logged in`);

        return {
            user: {
                id: user.id,
                username: user.username
            },
            tokens: {
                accessToken,
                refreshToken
            }
        };
    }


    async getUserFromDb(username: string) {
        return this.registryRepository.findOneBy({ username });
    }


    async getSignedTokens(userId: number, username: string) {
        const [ accessToken, refreshToken ] = await Promise.all([
            this.jwtService.signAsync(
                { id: userId, username },
                { secret: jwtConstants.secret, expiresIn: '10m' }
            ),
            this.jwtService.signAsync(
                { id: userId, username },
                { secret: jwtConstants.refreshSecret, expiresIn: '7d' }
            )
        ])
        return { accessToken, refreshToken };
    }


    async refreshTokens(userInfo: any) {

        const user = await this.registryRepository.findOneBy({ id: userInfo.id });
        if (user == null) throw new HttpException("User not in registry!", HttpStatus.CONFLICT);

        const match = await this.tokenRepository.findOneBy({ refreshToken: userInfo.token });
        if (match == null) throw new HttpException("Unknown token!", HttpStatus.CONFLICT);

        const { accessToken, refreshToken } = await this.getSignedTokens(userInfo.id, userInfo.username);

        this.updateTokenTable(user.id, refreshToken);

        console.log(`Tokens for user: ${user.username}, updated`);

        return { accessToken, refreshToken };
    }


    async updateTokenTable(id: number, newToken: string): Promise<void> {
        this.tokenRepository
        .createQueryBuilder()
        .update()
        .set({ refreshToken: newToken })
        .where({ userId: id })
        .execute();
    }
}
