import { Request } from 'express';
import { TokensDto } from 'src/auth/types/dtos/tokens.dto';
import { AuthService } from 'src/auth/services/auth/auth.service';
import { JwtRefreshAuthGuard } from 'src/auth/guards/jwtRefreshAuth.guard';
import { NewUserCredentials } from 'src/auth/types/dtos/newUserCredentials.dto';
import { LoggedUserCredentials } from 'src/auth/types/dtos/loggedUserCredentials.dto';
import { RegisteredUserCredentials } from 'src/auth/types/dtos/registeredUserCredentials.dto';
import { Body, Controller, Get, Inject, Post, Query, Req, UseGuards } from '@nestjs/common';
import { ValidateLoginPipe } from 'src/auth/pipes/validateLogin.pipe';




@Controller('auth')
export class AuthController {

    constructor(@Inject('AUTH_SERVICE') private readonly authService: AuthService) { }


    @Post()
    async register(
        @Body() userData: NewUserCredentials): Promise<LoggedUserCredentials> {
        return this.authService.registerUser(userData);
    }


    @Get()
    async login(
        @Query(ValidateLoginPipe) userData: RegisteredUserCredentials): Promise<LoggedUserCredentials> {
        return this.authService.loginUser(userData);
    }

 
    @Post('refresh')
    @UseGuards(JwtRefreshAuthGuard)
    async refresh(
        @Req() req: Request): Promise<TokensDto> {
        return this.authService.refreshTokens(req.user);
    }
}
