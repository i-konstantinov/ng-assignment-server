import { Module } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './services/auth/auth.service';
import { AuthController } from './controllers/auth/auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from './types/typeorm/user.entity';
import { TokenEntity } from './types/typeorm/token.entity';
import { JwtStrategy } from './guards/strategies/jwt.strategy';
import { JwtRefreshStrategy } from './guards/strategies/jwtRefresh.strategy';


@Module({
  imports: [
    PassportModule,
    JwtModule,
    TypeOrmModule.forFeature([ UserEntity, TokenEntity ])
  ],
  controllers: [ AuthController ],
  providers: [
    {
      provide: "AUTH_SERVICE",
      useClass: AuthService
    },
    JwtService,
    JwtStrategy,
    JwtRefreshStrategy
  ]
})
export class AuthModule { }
