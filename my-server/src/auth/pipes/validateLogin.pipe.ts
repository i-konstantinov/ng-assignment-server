import { HttpException, HttpStatus, Injectable, PipeTransform } from "@nestjs/common";
import { DataSource } from "typeorm";
import { RegisteredUserCredentials } from "../types/dtos/registeredUserCredentials.dto";
import { UserEntity } from "../types/typeorm/user.entity";

@Injectable()
export class ValidateLoginPipe implements PipeTransform {
    constructor(private dataSource: DataSource) { }

    async transform(data: RegisteredUserCredentials) {
        const user = await this.dataSource.createQueryBuilder(UserEntity, "Registry")
            .where({ username: data.username, password: data.password })
            .getOne();

        if (user) return data;
        throw new HttpException("Incorrect username or password!", HttpStatus.BAD_REQUEST);
    }
}
