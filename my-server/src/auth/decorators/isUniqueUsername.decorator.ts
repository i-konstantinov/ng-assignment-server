import { Injectable } from '@nestjs/common';
import {
    registerDecorator,
    ValidationOptions,
    ValidatorConstraint,
    ValidationArguments,
    ValidatorConstraintInterface,
} from 'class-validator';
import { DataSource } from 'typeorm';
import { UserEntity } from 'src/auth/types/typeorm/user.entity';



@Injectable()
@ValidatorConstraint({ async: true })
export class IsUniqueUsernameConstraint implements ValidatorConstraintInterface {

    constructor(private dataSource: DataSource) { }

    defaultMessage(): string {
        return "Username is occupied!";
    }

    async validate(value: number) {
        return this.dataSource
            .createQueryBuilder(UserEntity, "Registry")
            .where({ username: value })
            .getOne()
            .then(r => { if (r) return false; else return true; });
    };
}


export function isUniqueUsername(options?: any, validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string) {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [options],
            validator: IsUniqueUsernameConstraint
        });
    };
}
