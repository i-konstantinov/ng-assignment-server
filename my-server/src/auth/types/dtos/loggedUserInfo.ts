import { IsNumber, IsString } from 'class-validator';

export class LoggedUserInfo {
    @IsNumber()
    id: number;

    @IsString()
    username: string;
}