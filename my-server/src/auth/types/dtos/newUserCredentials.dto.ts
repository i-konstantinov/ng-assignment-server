import { IsNotEmpty, IsString } from 'class-validator';
import { isUniqueUsername } from 'src/auth/decorators/isUniqueUsername.decorator';

export class NewUserCredentials {
    @IsNotEmpty()
    @IsString()
    @isUniqueUsername()
    username: string;

    @IsNotEmpty()
    @IsString()
    password: string;
}