import { IsNotEmpty, IsString } from 'class-validator';
import { isValidEntity } from 'src/globalDecorators/isValidEntity.decorator';
import { UserEntity } from '../typeorm/user.entity';

export class RegisteredUserCredentials {
    @IsNotEmpty()
    @IsString()
    @isValidEntity({ tableName: "Registry",  errorMsg: "Incorrect username or password!", entity: UserEntity })
    username: string;

    @IsNotEmpty()
    @IsString()
    password: string;
}