import { IsNotEmptyObject } from 'class-validator';
import { LoggedUserInfo } from './loggedUserInfo';
import { TokensDto } from './tokens.dto';

export class LoggedUserCredentials {
    @IsNotEmptyObject()
    user: LoggedUserInfo
    
    @IsNotEmptyObject()
    tokens: TokensDto
}