import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";

@Entity({
    name: "Tokens",
    database: "ng_assignment_db",
    orderBy: { userId: "ASC" }
})
export class TokenEntity {
    @PrimaryGeneratedColumn({ type: "bigint"})
    id: number;

    @Column({
        nullable: false
    })
    refreshToken: string;

    @OneToOne(type => UserEntity, user => user.id, {
        onDelete: "CASCADE",
        nullable: false
    })
    @JoinColumn({
        name: "userId"
    })
    userId: number;
}