import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({
    name: "Registry",
    database: "ng_assignment_db",
    orderBy: { id: "ASC" }
})
export class UserEntity {
    @PrimaryGeneratedColumn({ type: 'bigint'})
    id: number;

    @Column({
        unique: true
    })
    username: string;

    @Column()
    password: string;
}