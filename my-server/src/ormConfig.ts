import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { TokenEntity } from "./auth/types/typeorm/token.entity";
import { UserEntity } from "./auth/types/typeorm/user.entity";
import { EmployeeEntity } from "./employees/types/typeorm/employee.entity";



export const typeOrmConfig: TypeOrmModuleOptions = {
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: 'ivan',
    password: 'ivan123',
    database: 'ng_assignment_db',
    entities: [ EmployeeEntity, UserEntity, TokenEntity ],
    synchronize: true,
    trace: true
};