import { Body, Controller, Delete, Get, Inject, Param, ParseIntPipe, Post, Put, Query, UseGuards } from '@nestjs/common';
import { EmployeeEntity } from 'src/employees/types/typeorm/employee.entity';
import { DeleteResult } from 'typeorm';
import { EmployeesService } from '../services/employees/employees.service';
import { EmployeeDto } from '../types/dtos/employee.dto';
import { JwtAuthGuard } from 'src/auth/guards/jwtAuth.guard';
import { SearchQuery } from '../types/dtos/searchQuery.dto';
import { Pagination } from 'nestjs-typeorm-paginate';
import { employeeIdDto } from '../types/dtos/employeeId.dto';




@Controller('employees')
@UseGuards(JwtAuthGuard)
export class EmployeesController {

    constructor(@Inject("EMPLOYEES_SERVICE") private employeesService: EmployeesService) { }

    
    @Get()
    async getEmployees(
        @Query() q: SearchQuery
    ): Promise<Pagination<EmployeeEntity>> {
        return this.employeesService.loadFiltered(q);
    }


    @Get('departments')
    async getDepartments(): Promise<string[]> {
        return this.employeesService.loadDepartments();
    }

    @Get('cities')
    async getCities(): Promise<string[]> {
        return this.employeesService.loadCities();
    }

    @Get('total')
    async getTotal(): Promise<number> {
        return this.employeesService.loadTotalNumberOfEmployees();
    }


    @Get(':id')
    async getEmployeeById(
        @Param() params: employeeIdDto,
    ): Promise<EmployeeEntity> {
        return this.employeesService.loadOneById(params.id);
    }


    @Get('user/:id')
    async getEmployeeByUserId(
        @Param("id", ParseIntPipe) userId: number,
    ): Promise<EmployeeEntity> {
        return this.employeesService.loadOneByUserId(userId);
    }


    @Post()
    async addEmployeeToCollection(
        @Body() newData: EmployeeDto
    ): Promise<EmployeeEntity> {
        return this.employeesService.saveEmployee(newData);
    }


    @Put()
    async updateEmployeeFromCollection(
        @Body() updatedData: EmployeeDto,
    ): Promise<EmployeeEntity> {
        return this.employeesService.updateEmployee(updatedData);
    }


    @Delete(':id')
    async deleteEmployeeFromCollection(
        @Param() params: employeeIdDto
    ): Promise<DeleteResult> {
        return this.employeesService.deleteEmployee(params.id);
    }
}