import { Module } from '@nestjs/common';
import { UserEntity } from 'src/auth/types/typeorm/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmployeesService } from './services/employees/employees.service';
import { EmployeesController } from './controllers/employees.controller';
import { IsValidIdConstraint } from '../globalDecorators/isValidEntity.decorator';
import { EmployeesRepository } from './employees.repository';
import { IsUniqueUsernameConstraint } from 'src/auth/decorators/isUniqueUsername.decorator';


@Module({
  imports: [
    TypeOrmModule.forFeature([ EmployeesRepository, UserEntity ])
  ],
  controllers: [ EmployeesController ],
  providers: [
    {
      provide: "EMPLOYEES_SERVICE",
      useClass: EmployeesService
    },
    IsValidIdConstraint,
    IsUniqueUsernameConstraint,
    EmployeesRepository
  ]
})
export class EmployeesModule { }