import { UserEntity } from "src/auth/types/typeorm/user.entity";
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from "typeorm";


@Entity({
    name: "Employees",
    database: "ng_assignment_db",
    orderBy: { id: "ASC" }
})
export class EmployeeEntity {
    @PrimaryGeneratedColumn({ type: 'bigint'})
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;

    @Column()
    position: string;

    @Column()
    salary: number;

    @Column({
        default: 'not set'
    })
    city: string;

    @Column({
        default: 'not set'
    })
    department: string;

    @Column({
        default: 'not set'
    })
    status: string;

    @OneToOne(type => UserEntity, user => user.id, {
        onDelete: "CASCADE",
        nullable: false, 
    })
    @JoinColumn({
        name: "userId"
    })
    userId: number;
}