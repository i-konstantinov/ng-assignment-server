import { IsNumberString, IsOptional, IsString } from "class-validator";


export class SearchQuery {
    @IsOptional()
    @IsString()
    department: string;

    @IsOptional()
    @IsString()
    city: string;

    @IsNumberString()
    page: number;

    @IsNumberString()
    limit: number;
}