import { isValidEntity } from "src/globalDecorators/isValidEntity.decorator";
import { EmployeeEntity } from "../typeorm/employee.entity";


export class employeeIdDto {

    @isValidEntity({tableName: "Employees", errorMsg: "Record does not exist!", entity: EmployeeEntity})
    id: number;

}