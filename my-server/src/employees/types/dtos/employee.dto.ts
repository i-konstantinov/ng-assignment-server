import { IsEmail, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';
import { isValidEntity } from 'src/globalDecorators/isValidEntity.decorator';
import { EmployeeEntity } from '../typeorm/employee.entity';



export class EmployeeDto {
    
    @IsOptional()
    @IsNumber()
    @isValidEntity({tableName: "Employees", errorMsg: "Record does not exist!", entity: EmployeeEntity})
    id: number

    @IsNotEmpty()
    @IsString()
    firstName: string;

    @IsNotEmpty()
    @IsString()
    lastName: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    @IsString()
    position: string;

    @IsNotEmpty()
    @IsNumber()
    salary: number;

    @IsNotEmpty()
    @IsNumber()
    userId: number;
    
    department ?: string;
    city ?: string;
    status ?: string;
}