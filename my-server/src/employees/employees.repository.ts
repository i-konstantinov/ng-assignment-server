import { Injectable } from "@nestjs/common";
import { DataSource, Repository } from "typeorm";
import { EmployeeDto } from "./types/dtos/employee.dto";
import { EmployeeEntity } from "./types/typeorm/employee.entity";


@Injectable()
export class EmployeesRepository {

    repository: Repository<EmployeeEntity>;

    constructor(private dataSource: DataSource) {
        this.repository = this.dataSource.getRepository(EmployeeEntity);
    }


    async findEmployeeById(providedId: number) {
        return await this.repository.findOne({
            loadRelationIds: true,
            where: { id: providedId }
        });
    }

    
    async findEmployeeByUserId(providedUserId: number) {
        return await this.repository
            .createQueryBuilder("employees")
            .loadAllRelationIds({ relations: ["userId"] })
            .where({ userId: providedUserId })
            .getOne();
    };


    async saveEmployee(employee: EmployeeDto) {
        const newRecord = this.repository.create(employee);
        return await this.repository.save(newRecord);
    }


    async updateEmployee(update: EmployeeDto) {
        const existing = await this.repository.findOne({ where: { id: update.id } });

        for (let prop in update) {
            if (prop == "id") continue;
            existing[prop] = update[prop];
        }

        return this.repository.save(existing);
    }


    async deleteEmployee(providedId: number) {
        return this.repository.delete({ id: providedId });
    }


    async countEmployees() { return this.repository.count() };


    async loadEmployeesDepartments() {
        return await this.repository
            .createQueryBuilder("employee")
            .select(["employee.department"])
            .getMany()
            .then(result => { return [...new Set(result.map(e => e["department"]))]; })
    }


    async loadEmployeesCities() {
        return await this.repository
            .createQueryBuilder("employee")
            .select(["employee.city"])
            .getMany()
            .then(result => { return [...new Set(result.map(e => e["city"]))]; })
    }
}