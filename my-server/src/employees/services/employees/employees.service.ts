import { Inject, Injectable } from '@nestjs/common';
import { SearchQuery } from 'src/employees/types/dtos/searchQuery.dto';
import { DataSource, DeleteResult } from 'typeorm';
import { EmployeeEntity } from 'src/employees/types/typeorm/employee.entity';
import { IPaginationOptions, paginate, Pagination } from 'nestjs-typeorm-paginate';
import { EmployeesRepository } from 'src/employees/employees.repository';
import { EmployeeDto } from 'src/employees/types/dtos/employee.dto';




@Injectable()
export class EmployeesService {

    constructor(
        @Inject(EmployeesRepository) private employeesRepository: EmployeesRepository,
        private dataSource: DataSource
    ) { }


    async loadFiltered(q: SearchQuery): Promise<Pagination<EmployeeEntity>> {
        const repo = this.dataSource.getRepository(EmployeeEntity);
        const paginationOptions: IPaginationOptions = { page: q.page, limit: q.limit };

        return paginate<EmployeeEntity>(repo, paginationOptions, {
            loadRelationIds: true,
            where: {
                department: q.department,
                city: q.city
            }
        });
    }


    async loadOneById(id: number): Promise<EmployeeEntity> {
        return this.employeesRepository.findEmployeeById(id);
    }


    async loadOneByUserId(id: number): Promise<EmployeeEntity> {
        return this.employeesRepository.findEmployeeByUserId(id);
    }


    
    async saveEmployee(employee: EmployeeDto): Promise<EmployeeEntity> {
        return this.employeesRepository.saveEmployee(employee);
    }


    async updateEmployee(updated: EmployeeDto): Promise<EmployeeEntity> {
        return this.employeesRepository.updateEmployee(updated);
    }


    async deleteEmployee(employeeId: number): Promise<DeleteResult> {
        return this.employeesRepository.deleteEmployee(employeeId);
    }


    async loadTotalNumberOfEmployees(): Promise<number> {
        return this.employeesRepository.countEmployees();
    }


    async loadDepartments(): Promise<string[]> {
        return await this.employeesRepository.loadEmployeesDepartments();
    }

    async loadCities(): Promise<string[]> {
        return await this.employeesRepository.loadEmployeesCities();
    }
    
}
